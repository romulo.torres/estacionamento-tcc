package br.com.torres.estacionamento.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.torres.estacionamento.model.entities.Usuario;
import br.com.torres.estacionamento.model.repository.Perfis;
import br.com.torres.estacionamento.model.repository.Usuarios;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController extends CrudController<Usuario> {

	@Autowired
	protected Usuarios usuarios;

	@Autowired
	protected Perfis perfis;

	@Autowired
	protected BCryptPasswordEncoder crypt;

	public Usuarios service() {
		return usuarios;
	}

	@Override
	public Usuario prePost(Usuario resource) {
		resource.setSenha(crypt.encode(resource.getSenha()));
		return super.prePost(resource);
	}

	@Override
	public Usuario prePut(Usuario resource) {
		String senha = resource.getSenha();
		if (senha != null && !senha.isEmpty()) {
			resource.setSenha(crypt.encode(senha));
		} else {
			resource.setSenha(usuarios.getSenhaById(resource.getId()));
		}

		return super.prePost(resource);
	}
}
