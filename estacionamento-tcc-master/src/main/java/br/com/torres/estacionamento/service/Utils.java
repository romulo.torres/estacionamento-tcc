package br.com.torres.estacionamento.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class Utils {
  public static Float calculaDiferenca(LocalDateTime inicio, LocalDateTime fim) {
    long diferencaEmMinutos = inicio.until(fim, ChronoUnit.MINUTES);
    Float diferencaEmHoras = (float) diferencaEmMinutos / 60;
    return diferencaEmHoras;
  }

  public static Float calculaDiferencaMinutos(LocalDateTime inicio, LocalDateTime fim) {
    long diferencaEmMinutos = inicio.until(fim, ChronoUnit.MINUTES);
    return (float) diferencaEmMinutos;
  }

  public static float round(Float d, int decimalPlace) {
    BigDecimal bd = new BigDecimal(Float.toString(d));
    bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
    return bd.floatValue();
  }
}
