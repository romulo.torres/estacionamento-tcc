package br.com.torres.estacionamento.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.torres.estacionamento.model.entities.Contrato;

@Repository
public interface Contratos extends JpaRepository<Contrato, Long> {

}
