package br.com.torres.estacionamento.controllers;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.torres.estacionamento.model.entities.Configuracao;
import br.com.torres.estacionamento.model.entities.PagamentoTicket;
import br.com.torres.estacionamento.model.entities.Ticket;
import br.com.torres.estacionamento.model.repository.Configuracoes;
import br.com.torres.estacionamento.model.repository.PagamentosTickets;
import br.com.torres.estacionamento.model.repository.Tickets;
import br.com.torres.estacionamento.service.Utils;

@RestController
@RequestMapping("/tickets")
public class TicketController extends CrudController<Ticket> {

	@Autowired
	protected Tickets tickets;
	@Autowired
	protected PagamentosTickets pagamentos;
	@Autowired
	protected Configuracoes configuracoes;
	@Autowired
	BCryptPasswordEncoder crypt;

	public Tickets service() {
		return tickets;
	}

	@PutMapping(value = "/{id}/pagar")
	@ResponseStatus(HttpStatus.OK)
	public Ticket pagar(@PathVariable("id") Long id) {

		LocalDateTime dataReferencia = getDataHora();
		Ticket t = tickets.getOne(id);
		t.pagar(dataReferencia);
		t = service().saveAndFlush(t);

		PagamentoTicket p = new PagamentoTicket();
		p.setReferenciaInicial(t.getEntrada());
		p.setReferenciaFinal(dataReferencia);
		p.setValorPago(t.getValor());
		p.setTicket(t);
		pagamentos.save(p);

		return service().saveAndFlush(t);
	}

	@PutMapping(value = "/{id}/validar")
	@ResponseStatus(HttpStatus.OK)
	public Ticket validar(@PathVariable("id") Long id) throws Exception {
		Ticket t = tickets.getOne(id);
		Configuracao config = configuracoes.findTopByOrderByIdDesc();
		if (t.getValor() != null
				&& Utils.calculaDiferencaMinutos(t.getSaida(), getDataHora()) < config.getMinutosTolerancia()) {
			t.setValidado(true);
		} else {
			throw new Exception("Não pode ser validado. Situação inválida.");
		}
		return service().saveAndFlush(t);
	}

	@Override
	public Ticket prePost(Ticket ticket) {
		Configuracao configuracao = configuracoes.findTopByOrderByIdDesc();
		ticket.setValorHora(configuracao.getValorHora());
		ticket.setEntrada(getDataHora());
		return super.prePost(ticket);
	}
}
