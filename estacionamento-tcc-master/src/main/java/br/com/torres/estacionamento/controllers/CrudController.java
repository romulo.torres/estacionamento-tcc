package br.com.torres.estacionamento.controllers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.torres.estacionamento.exceptions.ResourceNotFoundException;
import br.com.torres.estacionamento.model.entities.Base;

@RestController
public abstract class CrudController<T> {

    @Autowired
    HttpServletRequest request;

    protected abstract JpaRepository<T, Long> service();

    @GetMapping
    public List<T> findAll() {
        return service().findAll();
    }

    @GetMapping(value = "/{id}")
    public T findById(@PathVariable("id") Long id) {
        try {
            return service().findById(id).get();
        } catch (Exception e) {
            throw new ResourceNotFoundException();
        }
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public T create(@RequestBody T resource) {
        resource = prePost(resource);
        return service().save(resource);
    }

    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public T update(@PathVariable("id") Long id, @RequestBody T resource) {
        ((Base) resource).setId(id);
        resource = prePut(resource);
        return service().saveAndFlush(resource);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") Long id) {
        service().deleteById(id);
    }

    public T prePost(T resource) {
        return resource;
    }

    public T prePut(T resource) {
        return resource;
    }

    public LocalDateTime getDataHora() {
        String alteradoEmStr = request.getHeader("ReferenceDate");
        return LocalDateTime.parse(alteradoEmStr, DateTimeFormatter.ISO_DATE_TIME);
    }
}
