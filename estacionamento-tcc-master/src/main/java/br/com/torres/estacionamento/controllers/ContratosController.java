package br.com.torres.estacionamento.controllers;

import java.time.LocalDateTime;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.torres.estacionamento.exceptions.ResourceNotFoundException;
import br.com.torres.estacionamento.model.entities.Configuracao;
import br.com.torres.estacionamento.model.entities.Contrato;
import br.com.torres.estacionamento.model.entities.PagamentoContrato;
import br.com.torres.estacionamento.model.repository.Configuracoes;
import br.com.torres.estacionamento.model.repository.Contratos;
import br.com.torres.estacionamento.model.repository.PagamentosContratos;
import br.com.torres.estacionamento.model.responses.ResponseLiberacao;
import br.com.torres.estacionamento.model.responses.ResponsePagamento;

@RestController
@RequestMapping("/contratos")
public class ContratosController extends CrudController<Contrato> {

	@Autowired
	protected Contratos contratos;
	@Autowired
	protected PagamentosContratos pagamentos;
	@Autowired
	protected Configuracoes configuracoes;
	@Autowired
	BCryptPasswordEncoder crypt;

	public Contratos service() {
		return contratos;
	}

	@Override
	public Contrato prePost(Contrato contrato) {
		Configuracao configuracao = configuracoes.findTopByOrderByIdDesc();
		contrato.setValorMensal(configuracao.getValorMes());

		LocalDateTime dataReferencia = getDataHora();
		contrato.setInicioVigencia(dataReferencia.toLocalDate());
		contrato.setUltimaDataBase(dataReferencia.toLocalDate());
		contrato.setFimVigencia(dataReferencia.toLocalDate().plusMonths(1));
		return super.prePost(contrato);
	}

	@GetMapping(value = "/{id}/liberar")
	public ResponseLiberacao liberar(@PathVariable("id") Long id) {
		try {
			LocalDateTime dataReferencia = getDataHora();
			Contrato c = service().findById(id).get();
			return new ResponseLiberacao(c.getFimVigencia().isAfter(dataReferencia.toLocalDate()));
		} catch (NoSuchElementException e) {
			throw new ResourceNotFoundException();
		} catch (Exception e) {
			throw e;
		}
	}

	@PutMapping(value = "/{id}/pagar")
	public ResponsePagamento pagar(@PathVariable("id") Long id) {
		try {
			Contrato c = service().findById(id).get();
			PagamentoContrato p = new PagamentoContrato();

			p.setContrato(c);
			p.setReferenciaInicial(c.getUltimaDataBase());
			p.setReferenciaFinal(c.getFimVigencia());
			p.setValorPago(c.getValorMensal());

			LocalDateTime dataReferencia = getDataHora();
			c.setUltimaDataBase(dataReferencia.toLocalDate());
			c.setFimVigencia(dataReferencia.toLocalDate().plusMonths(1));

			pagamentos.save(p);
			contratos.save(c);

			return new ResponsePagamento(true);

		} catch (NoSuchElementException e) {
			throw new ResourceNotFoundException();
		} catch (Exception e) {
			throw e;
		}
	}
}
