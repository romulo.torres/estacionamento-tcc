package br.com.torres.estacionamento.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE)
public class ContratoVencidoException extends RuntimeException {
  @Override
  public String getMessage() {
    return "O contrato solicitado encontra-se vencido.";
  }

  private static final long serialVersionUID = -3357578235086772559L;
}