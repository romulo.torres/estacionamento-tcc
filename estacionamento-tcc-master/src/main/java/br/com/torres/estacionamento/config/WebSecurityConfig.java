package br.com.torres.estacionamento.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import br.com.torres.estacionamento.model.entities.Permissao;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

	@Autowired
	private UserDetailsService jwtUserDetailsService;

	@Autowired
	private JwtRequestFilter jwtRequestFilter;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder());
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.csrf().disable().authorizeRequests().antMatchers("/login").permitAll().antMatchers("/permissoes")
				.authenticated()

				.antMatchers(HttpMethod.GET, "/usuarios/**").hasAuthority(Permissao.USUARIOS_LISTAR.toString())
				.antMatchers(HttpMethod.POST, "/usuarios").hasAuthority(Permissao.USUARIOS_CRIAR.toString())
				.antMatchers(HttpMethod.PUT, "/usuarios/**").hasAuthority(Permissao.USUARIOS_EDITAR.toString())
				.antMatchers(HttpMethod.DELETE, "/usuarios/**").hasAuthority(Permissao.USUARIOS_EXCLUIR.toString())

				.antMatchers(HttpMethod.GET, "/perfis/**").hasAuthority(Permissao.PERFIS_LISTAR.toString())
				.antMatchers(HttpMethod.POST, "/perfis").hasAuthority(Permissao.PERFIS_CRIAR.toString())
				.antMatchers(HttpMethod.PUT, "/perfis/**").hasAuthority(Permissao.PERFIS_EDITAR.toString())
				.antMatchers(HttpMethod.DELETE, "/perfis/**").hasAuthority(Permissao.PERFIS_EXCLUIR.toString())

				.antMatchers(HttpMethod.GET, "/vagas/**").hasAuthority(Permissao.VAGAS_LISTAR.toString())
				.antMatchers(HttpMethod.POST, "/vagas").hasAuthority(Permissao.VAGAS_CRIAR.toString())
				.antMatchers(HttpMethod.PUT, "/vagas/**").hasAuthority(Permissao.VAGAS_EDITAR.toString())
				.antMatchers(HttpMethod.DELETE, "/vagas/**").hasAuthority(Permissao.VAGAS_EXCLUIR.toString())

				.anyRequest().authenticated().and().exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint)
				.and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

		httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
	}

}