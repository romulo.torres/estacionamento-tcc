package br.com.torres.estacionamento.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.torres.estacionamento.model.entities.Configuracao;

@Repository
public interface Configuracoes extends JpaRepository<Configuracao, Long> {
  public Configuracao findTopByOrderByIdDesc();
}
