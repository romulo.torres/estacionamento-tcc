package br.com.torres.estacionamento.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.torres.estacionamento.model.entities.Vaga;
import br.com.torres.estacionamento.model.repository.Vagas;

@RestController
@RequestMapping("/vagas")
public class VagaController extends CrudController<Vaga> {

	@Autowired
	protected Vagas vagas;
	@Autowired
	BCryptPasswordEncoder crypt;

	public Vagas service() {
		return vagas;
	}
}
