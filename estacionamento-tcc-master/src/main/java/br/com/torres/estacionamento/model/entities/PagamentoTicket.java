package br.com.torres.estacionamento.model.entities;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Where(clause = "excluido=false")
@SQLDelete(sql = "update PagamentoTicket SET excluido = 'true' where id=?")
@EqualsAndHashCode(callSuper = true)
public @Data class PagamentoTicket extends Base {

	@ManyToOne
	@JoinColumn(name = "ticket_id", referencedColumnName = "id")
	private Ticket ticket;

	private LocalDateTime referenciaInicial;

	private LocalDateTime referenciaFinal;

	private Float valorPago;

}
