package br.com.torres.estacionamento.model.responses;

import java.io.Serializable;

import lombok.Data;

public @Data class ResponseLiberacao implements Serializable {
  private static final long serialVersionUID = 3463470350355245967L;
  private Boolean liberado;

  public ResponseLiberacao(Boolean liberado) {
    this.liberado = liberado;
  }
}