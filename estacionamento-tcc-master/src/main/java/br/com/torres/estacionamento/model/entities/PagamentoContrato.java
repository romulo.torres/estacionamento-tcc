package br.com.torres.estacionamento.model.entities;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Where(clause = "excluido=false")
@SQLDelete(sql = "update PagamentoContrato SET excluido = 'true' where id=?")
@EqualsAndHashCode(callSuper = true)
public @Data class PagamentoContrato extends Base {

	@ManyToOne
	@JoinColumn(name = "contrato_id", referencedColumnName = "id")
	private Contrato contrato;

	private LocalDate referenciaInicial;

	private LocalDate referenciaFinal;

	private Float valorPago;

}
