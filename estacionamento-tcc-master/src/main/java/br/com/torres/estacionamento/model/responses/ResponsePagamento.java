package br.com.torres.estacionamento.model.responses;

import java.io.Serializable;
import lombok.Data;

public @Data class ResponsePagamento implements Serializable {
  private static final long serialVersionUID = 3463470350355245967L;
  private Boolean pago;

  public ResponsePagamento(Boolean pago) {
    this.pago = pago;
  }
}