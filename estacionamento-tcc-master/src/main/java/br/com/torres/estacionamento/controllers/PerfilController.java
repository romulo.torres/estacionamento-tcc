package br.com.torres.estacionamento.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.torres.estacionamento.model.entities.Perfil;
import br.com.torres.estacionamento.model.repository.Perfis;

@RestController
@RequestMapping("/perfis")
public class PerfilController extends CrudController<Perfil> {

	@Autowired
	protected Perfis perfis;
	@Autowired
	BCryptPasswordEncoder crypt;

	public Perfis service() {
		return perfis;
	}
}
