package br.com.torres.estacionamento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EstacionamentoApplication {

	public static void main(String[] args) {
		try {
			SpringApplication.run(EstacionamentoApplication.class, args);
		} catch (Throwable e) {
			if (e.getClass().getName().contains("SilentExitException")) {
				System.out.println("Spring is restarting the main thread - See spring-boot-devtools");
			} else {
				System.out.println("Application crashed! - " + e);
			}
		}
	}

}
