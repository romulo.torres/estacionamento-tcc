package br.com.torres.estacionamento.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.torres.estacionamento.model.entities.Usuario;

@Repository
public interface Usuarios extends JpaRepository<Usuario, Long> {

	public Usuario getByEmail(String email);

	@Query(value = "SELECT u.senha FROM Usuario u WHERE id = ?1")
	public String getSenhaById(Long id);
}
