package br.com.torres.estacionamento.model.entities;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@DynamicUpdate
@Where(clause = "excluido=false")
@SQLDelete(sql = "update Contrato SET excluido = 'true' where id=?")
@EqualsAndHashCode(callSuper = true)
public @Data class Contrato extends Base {

	@NotNull
	@ManyToOne
	@JoinColumn(name = "cliente_id", referencedColumnName = "id")
	private Cliente cliente;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "vaga_id", referencedColumnName = "id")
	private Vaga vaga;

	private LocalDate inicioVigencia;

	private LocalDate ultimaDataBase;

	private LocalDate fimVigencia;

	private Float valorMensal;

	private String codigo;

}