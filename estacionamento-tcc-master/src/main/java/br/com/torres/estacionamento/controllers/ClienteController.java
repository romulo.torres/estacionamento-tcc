package br.com.torres.estacionamento.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.torres.estacionamento.model.entities.Cliente;
import br.com.torres.estacionamento.model.repository.Clientes;

@RestController
@RequestMapping("/clientes")
public class ClienteController extends CrudController<Cliente> {

	@Autowired
	protected Clientes clientes;
	@Autowired
	BCryptPasswordEncoder crypt;

	public Clientes service() {
		return clientes;
	}
}
