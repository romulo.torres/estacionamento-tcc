package br.com.torres.estacionamento.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.torres.estacionamento.model.entities.Permissao;

@RestController
@RequestMapping("/permissoes")
public class PermissaoController {
    @GetMapping
    public List<Permissao> findAll() {
        Permissao[] permissoes = Permissao.values();
        List<Permissao> permissoesList = new ArrayList<Permissao>();
        for (Permissao permissao : permissoes) {
            permissoesList.add(permissao);
        }
        return permissoesList;
    }
}
