package br.com.torres.estacionamento.model.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Where(clause = "excluido=false")
@SQLDelete(sql = "update Perfil SET excluido = 'true' where id=?")
@EqualsAndHashCode(callSuper = true)
public @Data class Perfil extends Base {

	@NotBlank
	@Column(nullable = false)
	private String nome;

	@Enumerated(EnumType.STRING)
	@ElementCollection(targetClass = Permissao.class, fetch = FetchType.EAGER)
	private List<Permissao> permissao;

}
