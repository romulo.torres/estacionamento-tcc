package br.com.torres.estacionamento.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.torres.estacionamento.model.entities.Perfil;
import br.com.torres.estacionamento.model.entities.Permissao;
import br.com.torres.estacionamento.model.entities.Usuario;
import br.com.torres.estacionamento.model.repository.Usuarios;

@Service
public class JwtUserDetailsService implements UserDetailsService {

	@Autowired
	Usuarios usuarios;

	private Usuario usuario = null;

	public Usuario getUsuario() {
		return usuario;
	}

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

		usuario = usuarios.getByEmail(email);
		ArrayList<GrantedAuthority> privilegies = new ArrayList<>();
		privilegies.add(new GrantedAuthority() {

			private static final long serialVersionUID = 1L;

			@Override
			public String getAuthority() {
				return "USUARIOS_CRIAR";
			}
		});
		if (usuario != null) {
			return new User(usuario.getEmail(), usuario.getSenha(), !usuario.getExcluido(), true, true, true,
					getAuthorities(usuario.getPerfil()));
		} else {
			throw new UsernameNotFoundException("Usuário não encontrado: " + email);
		}
	}

	private Collection<? extends GrantedAuthority> getAuthorities(Perfil perfil) {
		return getGrantedAuthorities(getPrivileges(perfil));
	}

	private List<String> getPrivileges(Perfil perfil) {

		List<Permissao> collection = perfil != null ? perfil.getPermissao() : new ArrayList<Permissao>();

		List<String> privileges = new ArrayList<>();
		for (Permissao item : collection) {
			privileges.add(item.toString());
		}
		return privileges;
	}

	private List<GrantedAuthority> getGrantedAuthorities(List<String> privileges) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		for (String privilege : privileges) {
			authorities.add(new SimpleGrantedAuthority(privilege));
		}
		return authorities;
	}
}