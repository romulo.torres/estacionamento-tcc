package br.com.torres.estacionamento.model.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@EqualsAndHashCode(callSuper = true)
@Where(clause = "excluido=false")
@SQLDelete(sql = "update Cliente SET excluido = 'true' where id=?")
public @Data class Cliente extends Base {

	private String Nome;

	private String CpfCnpj;

	private String Telefone;

}