package br.com.torres.estacionamento.model.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@EqualsAndHashCode(callSuper = true)
@Where(clause = "excluido=false")
@SQLDelete(sql = "update Configuracao SET excluido = 'true' where id=?")
public @Data class Configuracao extends Base {

	private Float valorHora;
	private Float valorMes;
	private Float minutosTolerancia;

}
