
package br.com.torres.estacionamento.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Where(clause = "excluido=false")
@SQLDelete(sql = "update Vaga SET excluido = 'true' where id=?")
@EqualsAndHashCode(callSuper = true)
public @Data class Vaga extends Base {

	@NotBlank
	@Column(nullable = false)
	private String codigo;

	private Boolean exclusivaContrato;
}
