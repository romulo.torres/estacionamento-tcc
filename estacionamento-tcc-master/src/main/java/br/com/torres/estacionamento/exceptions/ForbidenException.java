package br.com.torres.estacionamento.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN)
public final class ForbidenException extends RuntimeException {
	private static final long serialVersionUID = -3357578235086772559L;

	public ForbidenException() {
		super("Operação não permitida");
	}
}