package br.com.torres.estacionamento.model.requests;

import java.time.LocalDateTime;

import lombok.Data;

public @Data class ReferenceDate {
  private LocalDateTime dataReferencia;
}
