package br.com.torres.estacionamento.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public final class ResourceNotFoundException extends RuntimeException {
	@Override
	public String getMessage() {
		return "O registro solicitado não foi encontrado na base de dados.";
	}

	private static final long serialVersionUID = -3357578235086772559L;
}