package br.com.torres.estacionamento.model.entities;

import java.time.LocalDateTime;

import javax.persistence.Entity;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import br.com.torres.estacionamento.service.Utils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Where(clause = "excluido=false")
@SQLDelete(sql = "update Ticket SET excluido = 'true' where id=?")
@EqualsAndHashCode(callSuper = true)
public @Data class Ticket extends Base {

	private String codigo;
	private Float valorHora;
	private LocalDateTime entrada;
	private LocalDateTime saida;
	private Float valor;
	private Boolean validado;

	public void pagar(LocalDateTime saida) {
		this.saida = saida;
		Float permanencia = Utils.calculaDiferenca(this.entrada, this.saida);
		this.valor = Utils.round(permanencia * this.valorHora, 2);
	}
}
