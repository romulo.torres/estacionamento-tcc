package br.com.torres.estacionamento.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.torres.estacionamento.exceptions.ForbidenException;
import br.com.torres.estacionamento.exceptions.ResourceNotFoundException;
import br.com.torres.estacionamento.model.entities.Configuracao;
import br.com.torres.estacionamento.model.repository.Configuracoes;

@RestController
@RequestMapping("/configuracoes")
public class ConfiguracoesController extends CrudController<Configuracao> {

	@Autowired
	protected Configuracoes configuracoes;

	public Configuracoes service() {
		return configuracoes;
	}

	@GetMapping(value = "/atual")
	public Configuracao atual() {
		try {
			return service().findTopByOrderByIdDesc();
		} catch (Exception e) {
			throw new ResourceNotFoundException();
		}
	}

	@PutMapping(value = "/{id}")
	@ResponseStatus(HttpStatus.FORBIDDEN)
	public Configuracao update(@PathVariable("id") Long id, @RequestBody Configuracao resource) {
		throw new ForbidenException();
	}
}
