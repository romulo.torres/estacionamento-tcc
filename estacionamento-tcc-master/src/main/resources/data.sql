create extension if not exists pgcrypto;

INSERT INTO public.perfil
  (excluido, nome)
VALUES(false, 'SuperAdmin');

INSERT INTO public.perfil_permissao
  (perfil_id, permissao)
VALUES
	(1, 'USUARIOS_CRIAR'),
	(1, 'USUARIOS_LISTAR'),
	(1, 'USUARIOS_EDITAR'),
	(1, 'USUARIOS_EXCLUIR'),
	(1, 'PERFIS_CRIAR'),
	(1, 'PERFIS_LISTAR'),
	(1, 'PERFIS_EDITAR'),
	(1, 'PERFIS_EXCLUIR'),
	(1, 'VAGAS_CRIAR'),
	(1, 'VAGAS_LISTAR'),
	(1, 'VAGAS_EDITAR'),
	(1, 'VAGAS_EXCLUIR'),
	(1, 'CLIENTES_CRIAR'),
	(1, 'CLIENTES_LISTAR'),
	(1, 'CLIENTES_EDITAR'),
	(1, 'CLIENTES_EXCLUIR'),
	(1, 'CONFIGURACOES_CRIAR'),
	(1, 'CONFIGURACOES_LISTAR'),
	(1, 'CONFIGURACOES_EDITAR'),
	(1, 'CONFIGURACOES_EXCLUIR'),
	(1, 'PAGAMENTOS_CRIAR'),
	(1, 'PAGAMENTOS_LISTAR'),
	(1, 'PAGAMENTOS_EDITAR'),
	(1, 'PAGAMENTOS_EXCLUIR'),
	(1, 'CONTRATOS_CRIAR'),
	(1, 'CONTRATOS_LISTAR'),
	(1, 'CONTRATOS_EDITAR'),
	(1, 'CONTRATOS_EXCLUIR'),
	(1, 'TICKETS_CRIAR'),
	(1, 'TICKETS_LISTAR'),
	(1, 'TICKETS_EDITAR'),
	(1, 'TICKETS_EXCLUIR');

INSERT INTO public.usuario
  (excluido, email, nome, senha, perfil_id)
VALUES(false, 'admin@dev.tcc', 'Super Administrador', crypt('123456', gen_salt('bf', 10)), 1);