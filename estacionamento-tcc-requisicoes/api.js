const Axios = require("axios");
const moment = require("moment");
const debug = require("./debug");

const objetos = {
  vagas: {},
  perfis: {},
  usuarios: {},
  clientes: {},
  contratos: {},
  configuracaoAtual: {},
  tokens: {},
  tickets: {},
};

const axios = Axios.create({
  baseURL: "http://127.0.0.1:8080",
});

let date = moment("2021-01-00 08:00:00", "YYYY-MM-DD HH:mm:ss");

// Funcões que executam as operações na API

function definirDataAtual(str) {
  date = moment(str);
  axios.defaults.headers.ReferenceDate = date.toISOString();
  debug.success(`DATA/HORA DEFINIDA PARA <${date.format("DD/MM/YYYY HH:mm")}>`);
}

function incrementaData(inc) {
  date = date.add(inc, "minutes");
  axios.defaults.headers.ReferenceDate = date.toISOString();
  debug.success(`DATA/HORA DEFINIDA PARA <${date.format("DD/MM/YYYY HH:mm")}>`);
}

function pegaData() {
  axios.defaults.headers.ReferenceDate = date.toISOString();
  return date.toISOString();
}

async function trocarUsuario(nome) {
  axios.defaults.headers.Authorization = `Bearer ${objetos.tokens[nome]}`;
  //axios.headers.Authorization = `Bearer ${objetos.tokens[nome]}`;
}

async function autenticar(nome, email, senha, erroEsperado = false) {
  const dataAtual = pegaData();
  try {
    const res = await axios.post("login", {
      email,
      senha,
    });
    objetos.tokens[nome] = res.data.token;
    trocarUsuario(nome);
    debug.success(
      `AUTENTICAÇÃO COM EMAIL: <${email}> e SENHA: <${senha}> COM SUCESSO`
    );
  } catch (e) {
    if (!erroEsperado) {
      debug.error(
        `TENTATIVA DE AUTENTICAÇÃO COM EMAIL: <${email}> e SENHA: <${senha}> COM FALHA`
      );
      throw e;
    } else {
      debug.success(
        `AUTENTICAÇÃO COM EMAIL: <${email}> e SENHA: <${senha}> NÃO AUTORIZADA CONFORME ESPERADO`
      );
    }
  }
}

async function autenticarUsuario(nome) {
  try {
    if (objetos.tokens[nome]) {
      trocarUsuario(nome);
    } else {
      await autenticar(
        nome,
        objetos.usuarios[nome].email,
        objetos.usuarios[nome].senha
      );
    }
  } catch (e) {
    debug.error(`ERRO AO AUTENTICAR USUÁRIO ${nome}`);
    debug.log(objetos.usuarios[nome]);
    throw e;
  }
}

async function criarConfiguracao(configuracoes) {
  try {
    const res = await axios.post("configuracoes", configuracoes);
    if (res)
      debug.success(
        `CONFIGURAÇÕES ALTERADAS: ${JSON.stringify(configuracoes)}`
      );
  } catch (e) {
    debug.error(`FALHA NA ATUALIZAÇÃO DE CONFIGURAÇÕES`);
    debug.log(e);
    throw e;
  }
}

async function criarVaga(parametros) {
  try {
    const res = await axios.post("vagas", parametros);
    objetos.vagas[parametros.codigo] = res.data;
    debug.success(`CRIADA VAGA: <${parametros.codigo}> COM SUCESSO`);
  } catch (e) {
    debug.error(`FALHA AO CRIAR VAGA: <${parametros.codigo}>`);
    debug.log(e);
    throw e;
  }
}

async function criarPerfil(parametros) {
  const alteradoEm = pegaData();
  try {
    const res = await axios.post("perfis", parametros);
    objetos.perfis[parametros.nome] = res.data;
    debug.success(`CRIADO PERFIL: <${parametros.nome}> COM SUCESSO`);
  } catch (e) {
    debug.error(`FALHA AO CRIAR PERFIL: <${parametros.perfil}>`);
    debug.log(e);
    throw e;
  }
}

async function criarUsuario(parametros) {
  const alteradoEm = pegaData();
  try {
    const res = await axios.post("usuarios", parametros);
    objetos.usuarios[parametros.nome] = {
      ...res.data,
      senha: parametros.senha,
    };
    debug.success(`CRIADO USUÁRIO: <${parametros.nome}> COM SUCESSO`);
  } catch (e) {
    debug.error(`FALHA AO CRIAR USUÁRIO: <${parametros.nome}>`);
    debug.log(e);
    throw e;
  }
}

async function criarCliente(parametros) {
  try {
    const res = await axios.post("clientes", parametros);
    objetos.clientes[parametros.nome] = res.data;
    debug.success(`CRIADO CLIENTE: <${parametros.nome}> COM SUCESSO`);
  } catch (e) {
    debug.error(`FALHA AO CRIAR CLIENTE: <${parametros.nome}>`);
    debug.log(e);
  }
}

async function criarContrato(parametros) {
  try {
    const res = await axios.post("contratos", parametros);
    objetos.contratos[parametros.codigo] = res.data;
    debug.success(`CRIADO CONTRATO: <${parametros.codigo}> COM SUCESSO`);
  } catch (e) {
    debug.error(`FALHA AO CRIAR CONTRATO: <${parametros.codigo}>`);
    debug.log(e);
    throw e;
  }
}

async function editarContrato(codigo, parametros) {
  const contrato = {
    ...objetos.contratos[codigo],
    ...parametros,
  };
  try {
    const res = await axios.put(`contratos/${contrato.id}`, contrato);
    objetos.contratos[codigo] = res.data;
    debug.success(`EDITADO CONTRATO: <${codigo}> COM SUCESSO`);
  } catch (e) {
    debug.error(`FALHA AO EDITAR CONTRATO: <${codigo}>`);
    debug.log(e);
    throw e;
  }
}

async function editarPerfil(nome, parametros) {
  const perfil = {
    ...objetos.perfis[nome],
    ...parametros,
  };
  try {
    const res = await axios.put(`perfis/${perfil.id}`, perfil);
    objetos.perfis[nome] = res.data;
    debug.success(`EDITADO PERFIL: <${nome}> COM SUCESSO`);
  } catch (e) {
    debug.error(`FALHA AO EDITAR PERFIL: <${nome}>`);
    debug.log(e);
    throw e;
  }
}

async function removerContrato(codigo) {
  const id = objetos.contratos[codigo].id;
  try {
    const res = await axios.delete(`contratos/${id}`);
    objetos.contratos[codigo] = res.data;
    debug.success(`EDITADO REMOVIDO: <${codigo}> COM SUCESSO`);
  } catch (e) {
    debug.error(`FALHA AO REMOVER CONTRATO: <${codigo}>`);
    debug.log(e);
    throw e;
  }
}

async function gerarTicket(parametros) {
  try {
    const res = await axios.post("tickets", parametros);
    objetos.tickets[parametros.codigo] = res.data;
    debug.success(`CRIADO TICKET: <${parametros.codigo}> COM SUCESSO`);
  } catch (e) {
    debug.error(`FALHA AO CRIAR TICKET: <${parametros.codigo}>`);
    debug.log(e);
    throw e;
  }
}

async function pagarTicket(codigo) {
  try {
    const ticket = objetos.tickets[codigo];
    const res = await axios.put(`tickets/${ticket.id}/pagar`);
    objetos.tickets[codigo] = res.data;
    debug.success(`PAGO TICKET: <${codigo}> COM SUCESSO`);
  } catch (e) {
    debug.error(`FALHA AO PAGAR TICKET: <${codigo}>`);
    debug.log(e);
    throw e;
  }
}

async function validarTicket(codigo) {
  try {
    const ticket = objetos.tickets[codigo];
    const res = await axios.put(`tickets/${ticket.id}/validar`);
    objetos.tickets[codigo] = res.data;
    debug.success(`VALIDADO TICKET: <${codigo}> COM SUCESSO`);
  } catch (e) {
    debug.error(`FALHA AO VALIDAR TICKET: <${codigo}>`);
    debug.log(e);
    throw e;
  }
}

async function liberarContrato(codigo, esperado) {
  try {
    const contrato = objetos.contratos[codigo];
    const res = await axios.get(`contratos/${contrato.id}/liberar`);
    if (res.data.liberado === esperado) {
      if (esperado) {
        debug.success(`LIBERADO CONTRATO: <${codigo}> COM SUCESSO`);
      } else {
        debug.success(`BLOQUEADO CONTRATO: <${codigo}> COM SUCESSO`);
      }
    } else {
      throw new Error(
        `>>> ESPERADO (${esperado}), AVALIADO (${res.data.liberado}) <${codigo}>`
      );
    }
  } catch (e) {
    debug.error(`FALHA NA AVALIAÇÃO DO CONTRATO <${codigo}>`);
    debug.log(e);
    throw e;
  }
}

async function pagarContrato(codigo) {
  try {
    const contrato = objetos.contratos[codigo];
    const res = await axios.put(`contratos/${contrato.id}/pagar`);
    debug.success(`PAGO CONTRATO: <${codigo}> COM SUCESSO`);
  } catch (e) {
    debug.error(`FALHA NO PAGAMENTO DO CONTRATO <${codigo}>`);
    debug.log(e);
    throw e;
  }
}

module.exports = {
  objetos,
  definirDataAtual,
  incrementaData,
  autenticar,
  autenticarUsuario,
  criarConfiguracao,
  criarVaga,
  criarPerfil,
  editarPerfil,
  criarUsuario,
  criarCliente,
  criarContrato,
  editarContrato,
  removerContrato,
  gerarTicket,
  pagarTicket,
  validarTicket,
  liberarContrato,
  pagarContrato,
};
