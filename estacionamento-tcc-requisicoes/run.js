const {
  objetos,
  definirDataAtual,
  incrementaData,
  autenticar,
  autenticarUsuario,
  criarConfiguracao,
  criarVaga,
  criarPerfil,
  editarPerfil,
  criarUsuario,
  criarCliente,
  criarContrato,
  editarContrato,
  removerContrato,
  gerarTicket,
  pagarTicket,
  validarTicket,
  liberarContrato,
  pagarContrato,
} = require("./api");

const timestamp = new Date().getTime();

// Método Principal
(async function exec() {
  console.log("");
  console.log("==========================================================");
  console.log("           INICIANDO SEQUENCIA DE REQUISIÇÕES");
  console.log("==========================================================");
  console.log("");

  // Data do sistema definido para 01/01/2021 08:00:00;
  definirDataAtual("2021-01-01T08:00:00");

  // Tentativa de autenticação com dados inválidos;
  await autenticar("naoExiste", "naoexiste@dev.tcc", "123456", true);

  // Alterada a hora do sistema para 08:01:00;
  definirDataAtual("2021-01-01T08:01:00");

  // Autenticação com dados válidos na conta de <Administrador>;
  await autenticar("Administrador", "admin@dev.tcc", "123456");

  // Alterada a hora do sistema para 08:10:00;
  definirDataAtual("2021-01-01T08:10:00");

  // Administrador cria um total de 10 (dez) Vagas (<Vaga 01>, <Vaga 02> … <Vaga 10>),
  // sendo incrementadas as horas do sistema em 1 minuto a cada criação;
  await criarVaga({ codigo: `01-C-${timestamp}`, exclusivaContrato: true });
  incrementaData(1);
  await criarVaga({ codigo: `02-C-${timestamp}`, exclusivaContrato: true });
  incrementaData(1);
  await criarVaga({ codigo: `03-C-${timestamp}`, exclusivaContrato: true });
  incrementaData(1);
  await criarVaga({ codigo: `04-C-${timestamp}`, exclusivaContrato: true });
  incrementaData(1);
  await criarVaga({ codigo: `05-C-${timestamp}`, exclusivaContrato: true });
  incrementaData(1);
  await criarVaga({ codigo: `06-M-${timestamp}`, exclusivaContrato: false });
  incrementaData(1);
  await criarVaga({ codigo: `07-M-${timestamp}`, exclusivaContrato: false });
  incrementaData(1);
  await criarVaga({ codigo: `08-M-${timestamp}`, exclusivaContrato: false });
  incrementaData(1);
  await criarVaga({ codigo: `09-M-${timestamp}`, exclusivaContrato: false });
  incrementaData(1);
  await criarVaga({ codigo: `10-M-${timestamp}`, exclusivaContrato: false });

  // Alterada a hora do sistema para 08:28:00;
  definirDataAtual("2021-01-01T08:28:00");

  // <Administrador> Altera as configurações do sistema para:
  // - Valor Hora Avulso: R$ 5,00;
  // - Valor Mensal: R$ 200,00;
  // - Tolerância: 10 minutos;
  await criarConfiguracao({
    valorHora: 5.0,
    valorMes: 200.0,
    minutosTolerancia: 10.0,
  });

  // Alterada a hora do sistema para 08:30:00;
  definirDataAtual("2021-01-01T08:30:00");

  // <Administrador> cria um novo perfil <Perfil 01>, definindo as seguintes permissões:
  // - Contratos: Listar, Cadastrar, Editar e Excluir;
  await criarPerfil({
    nome: `Perfil 01-${timestamp}`,
    permissao: [
      "CONTRATOS_CRIAR",
      "CONTRATOS_LISTAR",
      "CONTRATOS_EDITAR",
      "CONTRATOS_EXCLUIR",
    ],
  });

  // Alterada a hora do sistema para 08:30:05;
  definirDataAtual("2021-01-01T08:30:05");

  // <Administrador> cria um novo perfil <Cancela>.
  // Este Perfil será utilizado pelo sistema da cancela, sendo operado por computador.
  // O perfil deverá possuir as seguintes permissões:
  // - Ticket Avulso: Listar, Cadastrar, Editar;
  // - Configurações: Listar;
  await criarPerfil({
    nome: `Cancela-${timestamp}`,
    permissao: [
      "TICKETS_CRIAR",
      "TICKETS_LISTAR",
      "TICKETS_EDITAR",
      "CONFIGURACOES_LISTAR",
    ],
  });

  // Alterada a hora do sistema para 08:30:15;
  definirDataAtual("2021-01-01T08:30:15");

  // <Administrador> cria um novo perfil <Guichê 01>.
  // Este perfil será utilizado pelo sistema da máquina de pagamento, sendo operado por computador.
  // O perfil deverá possuir as seguintes permissões:
  // - Ticket Avulso: Listar, Editar;
  // - Configurações: Listar;

  await criarPerfil({
    nome: `Guichê-${timestamp}`,
    permissao: ["TICKETS_LISTAR", "TICKETS_EDITAR", "CONFIGURACOES_LISTAR"],
  });

  // Alterada a hora do sistema para 08:32:00;
  definirDataAtual("2021-01-01T08:32:00");

  // <Administrador> cria um novo usuário <Usuário 01>, com perfil <Perfil 01>;
  await criarUsuario({
    nome: `Usuário 01-${timestamp}`,
    email: `usuario01_${timestamp}@email.com`,
    senha: "usuario01",
    perfil: objetos.perfis[`Perfil 01-${timestamp}`],
  });

  // Alterada a hora do sistema para 08:32:10;
  definirDataAtual("2021-01-01T08:32:10");

  // <Administrador> cria um novo usuário <Cancela Entrada>, com perfil <Cancela>;
  await criarUsuario({
    nome: `Cancela Entrada-${timestamp}`,
    email: `cancelaentrada_${timestamp}@email.com`,
    senha: `cancelaentrada-${timestamp}`,
    perfil: objetos.perfis[`Cancela-${timestamp}`],
  });

  // Alterada a hora do sistema para 08:32:12;
  definirDataAtual("2021-01-01T08:32:12");

  // <Administrador> cria um novo usuário <Cancela Saída>, com perfil <Cancela>;
  await criarUsuario({
    nome: `Cancela Saída-${timestamp}`,
    email: `cancelasaida_${timestamp}@email.com`,
    senha: `cancelasaida-${timestamp}`,
    perfil: objetos.perfis[`Cancela-${timestamp}`],
  });

  // Alterada a hora do sistema para 08:32:14;
  definirDataAtual("2021-01-01T08:32:14");

  // <Administrador> cria um novo usuário <Guichê 01>, com perfil <Guichê>;
  await criarUsuario({
    nome: `Guichê 01-${timestamp}`,
    email: `guiche_${timestamp}@email.com`,
    senha: `guiche-${timestamp}`,
    perfil: objetos.perfis[`Guichê-${timestamp}`],
  });

  // Alterada a hora do sistema para 08:34:00;
  definirDataAtual("2021-01-01T08:34:00");

  // Autenticação do <Usuário 01>;
  await autenticarUsuario(`Usuário 01-${timestamp}`);

  // Alterada a hora do sistema para 08:40:00;
  definirDataAtual("2021-01-01T08:40:00");

  // <Usuário 01> cria um novo Cliente <Cliente 01>;
  await criarCliente({
    nome: `Cliente 01-${timestamp}`,
    CpfCnpj: "000.000.000.01",
    telefone: "00-0000-0001",
  });

  // Alterada a hora do sistema para 08:42:00;
  definirDataAtual("2021-01-01T08:42:00");

  // <Usuário 01> cria um novo Contrato <Contrato 01> para o <Cliente 01>;
  await criarContrato({
    codigo: `Contrato 01-${timestamp}`,
    cliente: objetos.clientes[`Cliente 01-${timestamp}`],
    vaga: objetos.vagas[`01-C-${timestamp}`],
  });

  // Alterada a hora do sistema para 08:43:00;
  definirDataAtual("2021-01-01T08:43:00");

  // <Usuário 01> edita o <Contrato 01>;
  await editarContrato(`Contrato 01-${timestamp}`, {
    valorMensal: 150.0,
  });

  // Alterada a hora do sistema para 08:44:00;
  definirDataAtual("2021-01-01T08:44:00");

  // <Usuário 01> exclui o <Contrato 01>
  await removerContrato(`Contrato 01-${timestamp}`);

  // Alterada a hora do sistema para 08`47:-${timestamp}`)0;
  definirDataAtual("2021-01-01T08:47:00");

  // Administrador Altera o <Perfil 01> removendo do perfil a permissão “Excluir Contrato”;
  await autenticarUsuario(`Administrador`);
  await editarPerfil(`Perfil 01-${timestamp}`, {
    permissao: ["CONTRATOS_CRIAR", "CONTRATOS_LISTAR", "CONTRATOS_EDITAR"],
  });

  // Alterada a hora do sistema para 08:50:00;
  definirDataAtual("2021-01-01T08:50:00");

  // <Usuário 01> cria um novo Cliente <Cliente 02>;
  await autenticarUsuario(`Usuário 01-${timestamp}`);
  await criarCliente({
    nome: `Cliente 02-${timestamp}`,
    CpfCnpj: "000.000.000.02",
    telefone: "00-0000-0002",
  });

  // Alterada a hora do sistema para 08:51:00;
  definirDataAtual("2021-01-01T08:51:00");

  // <Usuário 01> cria um novo Contrato <Contrato 02> para o <Cliente 02>;
  await criarContrato({
    codigo: `Contrato 02-${timestamp}`,
    cliente: objetos.clientes[`Cliente 02-${timestamp}`],
    vaga: objetos.vagas[`02-C-${timestamp}`],
  });

  // Alterada a hora do sistema para 08:53:00;
  definirDataAtual("2021-01-01T08:53:00");

  // <Usuário 01> cria um novo Contrato <Contrato 01>;
  await criarContrato({
    codigo: `Contrato 01-${timestamp}`,
    cliente: objetos.clientes[`Cliente 01-${timestamp}`],
    vaga: objetos.vagas[`01-C-${timestamp}`],
  });

  // Alterada a hora do sistema para 08:55:00;
  definirDataAtual("2021-01-01T08:55:00");

  // <Usuário 01> edita o <Contrato 01>;
  await editarContrato(`Contrato 01-${timestamp}`, {
    valorMensal: 150.0,
  });

  // Alterada a hora do sistema para 08:56:00;
  definirDataAtual("2021-01-01T08:56:00");

  // <Usuário 01> exclui o <Contrato 01>; (não deve possuir permissão para tal nesse momento);
  await removerContrato(`Contrato 01-${timestamp}`);

  // Alterada a hora do sistema para 08`57:-${timestamp}`)0;
  definirDataAtual("2021-01-01T08:57:00");

  // <Administrador> Altera o <Perfil 01> para possuir as seguintes permissões:
  // Contratos: Listar, Cadastrar, Editar e Excluir;
  // Configurações: Listar, Cadastrar, Editar e Excluir;
  await autenticarUsuario(`Administrador`);
  await editarPerfil(`Perfil 01-${timestamp}`, {
    permissao: [
      "CONTRATOS_CRIAR",
      "CONTRATOS_LISTAR",
      "CONTRATOS_EDITAR",
      "CONTRATOS_EXCLUIR",
      "CONFIGURACOES_CRIAR",
      "CONFIGURACOES_LISTAR",
      "CONFIGURACOES_EDITAR",
      "CONFIGURACOES_EXCLUIR",
    ],
  });

  // Alterada a hora do sistema para 09:00:00;
  definirDataAtual("2021-01-01T09:00:00");

  // Autenticação do usuário <Cancela Entrada>;
  await autenticarUsuario(`Cancela Entrada-${timestamp}`);

  // <Cancela Entrada> gera Ticket Avulso <Ticket Avulso 01>;
  await gerarTicket({ codigo: `Ticket Avulso 01-${timestamp}` });

  // Alterada a hora do sistema para 09:29:00;
  definirDataAtual("2021-01-01T09:29:00");

  // <Usuário 01> Altera configurações para definir:
  // Valor Hora Avulso: R$ 1,00
  // - [simulando uma fraude]: o <Usuário 01> está alterando a configuração do sistema para gerar
  //                           um ticket de valor mais baixo que o definido pelo administrador;
  await autenticarUsuario(`Usuário 01-${timestamp}`);
  await criarConfiguracao({
    valorHora: 1.0,
    valorMes: 200.0,
    minutosTolerancia: 10.0,
  });

  // Alterada a hora do sistema para 09:30:00;
  definirDataAtual("2021-01-01T09:30:00");

  // <Cancela Entrada> gera Ticket Avulso <Ticket Avulso 02> (este ticket será gerado com as configurações fraudadas).
  await autenticarUsuario(`Cancela Entrada-${timestamp}`);
  await gerarTicket({ codigo: `Ticket Avulso 02-${timestamp}` });

  // Alterada a hora do sistema para 09:31:00;
  definirDataAtual("2021-01-01T09:31:00");

  // <Usuário 01> Altera configurações para definir:
  // Valor Hora Avulso: R$ 5,00 (tentando encobrir a fraude, o <Usuário 01> retorna as configurações
  //                    como estava anteriormente.
  await autenticarUsuario(`Usuário 01-${timestamp}`);
  await criarConfiguracao({
    valorHora: 5.0,
    valorMes: 200.0,
    minutosTolerancia: 10.0,
  });

  // Alterada a hora do sistema para 10:00:00;
  definirDataAtual("2021-01-01T10:00:00");

  // <Guichê 01> Registra pagamento do <Ticket Avulso 01>;
  await autenticarUsuario(`Guichê 01-${timestamp}`);
  await pagarTicket(`Ticket Avulso 01-${timestamp}`);

  // Alterada a hora do sistema para 10:01:00;
  definirDataAtual("2021-01-01T10:01:00");

  // <Cancela Saída> Valida <Ticket Avulso 01>;
  await autenticarUsuario(`Cancela Saída-${timestamp}`);
  await validarTicket(`Ticket Avulso 01-${timestamp}`);

  // Alterada a hora do sistema para 10:30:00;
  definirDataAtual("2021-01-01T10:30:00");

  // <Guichê 01> Registra pagamento do <Ticket Avulso 02>;
  await autenticarUsuario(`Guichê 01-${timestamp}`);
  await pagarTicket(`Ticket Avulso 02-${timestamp}`);

  // Alterada a hora do sistema para 10:31:00;
  definirDataAtual("2021-01-01T10:31:00");

  // <Cancela Saída> Valida <Ticket Avulso 02>;
  await autenticarUsuario(`Cancela Saída-${timestamp}`);
  await validarTicket(`Ticket Avulso 02-${timestamp}`);

  // Alterada a data do sistema para 02/02/2016 08:00:00;
  definirDataAtual("2021-02-02T08:00:00");

  // <Cancela Entrada> Bloqueia entrada do <Contrato 02> por vencimento
  await autenticarUsuario(`Cancela Entrada-${timestamp}`);
  await liberarContrato(`Contrato 02-${timestamp}`, false);

  // Alterada a hora do sistema para 08:02:00;
  definirDataAtual("2021-02-02T08:00:00");

  // <Usuário 01> Altera <Contrato 01> para definir:
  // Valor Mensal: 2,00;
  await autenticarUsuario(`Usuário 01-${timestamp}`);
  await editarContrato(`Contrato 02-${timestamp}`, {
    valorMensal: 2.0,
  });

  // Alterada a hora do sistema para 08:03:00;
  definirDataAtual("2021-02-02T08:03:00");

  // <Guichê 01> Registra pagamento mensal do <Contrato 02>;
  await autenticarUsuario(`Guichê 01-${timestamp}`);
  await pagarContrato(`Contrato 02-${timestamp}`);

  // Alterada a hora do sistema para 08:04:00;
  definirDataAtual("2021-02-02T08:04:00");

  // <Cancela Entrada> Autoriza entrada do <Contrato 02>;
  await autenticarUsuario(`Cancela Entrada-${timestamp}`);
  await liberarContrato(`Contrato 02-${timestamp}`, true);

  // Alterada a hora do sistema para 08:05:00;
  definirDataAtual("2021-02-02T08:05:00");

  // <Usuário 01> Altera <Contrato 02> para definir:
  // Valor Mensal: 200,00;
  await editarContrato(`Contrato 02-${timestamp}`, {
    valorMensal: 200.0,
  });

  console.log("");
  console.log("==========================================================");
  console.log("           SEQUENCIA DE REQUISIÇÕES CONCLUÍDA");
  console.log("==========================================================");
  console.log("");
})();
