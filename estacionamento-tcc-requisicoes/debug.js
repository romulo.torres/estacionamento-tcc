const colors = {
  RESET: "\u001B[0m",
  BLACK: "\u001B[30m",
  RED: "\u001B[31m",
  GREEN: "\u001B[32m",
  YELLOW: "\u001B[33m",
  BLUE: "\u001B[34m",
  PURPLE: "\u001B[35m",
  CYAN: "\u001B[36m",
  WHITE: "\u001B[37m",
};

module.exports = {
  log: (...args) => console.log(...args),
  info: (txt) => console.log(`${colors.YELLOW}INFO${colors.RESET} - ${txt}`),
  error: (txt) => console.log(`${colors.RED}ERRO${colors.RESET} - ${txt}`),
  success: (txt) => console.log(`${colors.GREEN}OK${colors.RESET} - ${txt}`),
};
